# NABBY

YouTube mirror written in Python. What's it useful for, you ask? See "Potential use cases".

While it's using asyncio and sanic and has some limitations to prevent people from abusing public instances, it's not really designed to be used by too many people (or on too many videos) at once as it still has many sync parts and a json database (oof).

---

## Potential use cases

- Bypass censorship and region locks
- Avoid google tracking and analytics
- Avoid ads
- Avoid giving 1+ views or ad revenue to a video*
- Use youtube without javascript

*: especially useful when you're linking a bad video (see "About the name") but don't want them to support the creator of the bad video

---

## About the name

Name is the initials of this shitty video I don't want to give views or ad revenue to:

![YouTube video cover reading "Netflix Algoritmasi Beyninizi BOYLE Yikiyor"](https://awo.oooooooooooooo.ooo/i/2rgd5ppm.png)

(translates to "Netflix Algorithm Washes Your Brain LIKE THIS")

---

## How to run

- Install python3.6+
- Install required packages (`pip install -Ur requirements.txt`)
- Install `ffmpeg` as it'll be necessary to combine files
- Rename `config.py.template` to `config.py` and configure it if you want
- Run `nabby.py`
- To watch a youtube video on nabby, replace the `youtube.com` or `youtu.be` on URL with nabby's page (127.0.0.1:8000 by default)
